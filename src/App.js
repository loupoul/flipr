import Grid from './Components/Grid.jsx';

import './App.scss';

function App() {
   return (
      <div className="App">
         <Grid
            rowCount={ 6 }
            columnCount={ 6 }
         />
      </div >
   );
}

export default App;
