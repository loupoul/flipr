import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";

import Card from "./Card";
import InfoCard from "./InfoCard";

import './Grid.scss';

function Grid({ rowCount, columnCount }) {

   const [grid, setGrid] = useState([]);

   useEffect(() => {
      let grid = [];
      for (let col = 0; col < columnCount; col++) {
         const currentCol = [];
         for (let row = 0; row < columnCount; row++) {
            if (row === 5 && col === 5) {
               currentCol.push();
            } else if (row === 5 || col === 5) {
               currentCol.push(<InfoCard
                  nbBombs={ row }
                  nbPoints={ col }
               />);
            } else {
               currentCol.push(<Card
                  text={ row + col }
               />);
            }
         }
         grid.push(currentCol);
      }
      setGrid(grid);
   }, [rowCount, columnCount]);


   return (
      <div className="grid">
         { grid }
      </div>
   );
}

Grid.propTypes = {
   /** Number of rows */
   rowCount: PropTypes.number,
   /** Number of columns */
   columnCount: PropTypes.number,
};

export default Grid;
