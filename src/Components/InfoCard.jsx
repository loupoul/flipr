import './InfoCard.scss';

function InfoCard({ nbBombs, nbPoints }) {
   return (
      <div className="infoCard">
         <label className="infoCard__nb-bombs">{ nbBombs }</label>
         <label className="infoCard__nb-points">{ nbPoints }</label>
      </div >
   );
}


export default InfoCard;
